<?php
/*
	class: verifica sesion
*/
	class Access {

		private $CI;

		function __construct(){
			$this->CI = & get_instance();
		}
		
		function identified(){

			$controllersprivados = array('dashboard');

			if($this->CI->session->userdata('logged') == true){
				
				if($this->CI->router->method == 'logout'){
					
				}else if($this->CI->router->class == 'home'){
					redirect('index.php/dashboard');
				}else{
					#redirect(base_url());
				}
			}else if($this->CI->session->userdata('logged') != true){
				if($this->CI->router->class != 'home' && in_array($this->CI->router->class, $controllersprivados)){
					redirect(base_url());	
				}else if($this->CI->router->method == 'logout'){
					redirect(base_url());
				}
			}
		}
	}

?>