<?php if ( ! defined('BASEPATH')) exit('Forbbiden');

	class Login extends CI_Controller {

		public function __construct () {

			parent::__construct();
			$this->load->library("form_validation");
			$this->load->helper("form");

		}

		public function verify () {

			if (isset($_POST['email']) && isset($_POST['password'])) {
				
				$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
				$user = $_POST['email'];
				$pass = $_POST['password'];

				$this->form_validation->set_rules('email', 'Email field', 'trim|required|valid_email');
				$this->form_validation->set_rules('password', 'Password Field', 'trim|required');

				if($this->form_validation->run()==FALSE) { 
					$data["error_form"] = true;
					$this->load->vars($data);
					$this->load->view("index_view");
				}else {

					$this->load->model("login_model");

					$data_user = array(
						"email"	=>	$user,
						"password"	=>	$pass
					);

					$res = $this->login_model->verify($data_user);

					if ($res != false) {

						$user_data = array(
							"logged"	=> true,
							"name"	=> $res[0]->name,
							"id_user"=> $res[0]->id

						);

						$this->session->set_userdata($user_data);
						redirect("index.php/dashboard");
					} else {
						$data["user_error"] = true;
						$this->load->vars($data);
						$this->load->view("index_view");
					}

				}
			}else{
				redirect("/");
			}
		}

		public function end() {
			$this->session->unset_userdata();
			$this->session->sess_destroy();
			redirect("/");
		}

	}

?>