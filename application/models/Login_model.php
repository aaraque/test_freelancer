<?php if ( ! defined('BASEPATH')) exit('No se permite acceso directo al script');

	class Login_model extends CI_Model {

		#constructor de clase
		public function __contruct () {
			parent::__contruct();
		}

		public function verify ($data = array()) {

			if (count($data) == 0) {
				return false;
			}else {
				$res = $this->db->get_where("users",$data);
				if($res->num_rows() > 0){ 
					return $res->result(); 
				} else { 
					return false; 
				}
			}
		}

	}

?>